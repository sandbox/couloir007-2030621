<?php

/**
 * @file
 * Define Nodequeue Auto Rotate drush commands.
 */

/**
 * Implements hook_drush_help().
 */
function nodequeue_auto_rotate_drush_help($command) {
  switch ($command) {
    case 'drush:nodequeue-auto-rotate-cron':
      return dt('Runs cron rotate function: good for testing!!!');
  }
}

/**
 * Implements hook_drush_command().
 */
function nodequeue_auto_rotate_drush_command() {
  $items = [];

  $items['nodequeue-auto-rotate-cron'] = [
    'description' => dt('Runs cron rotate function.'),
    'examples' => [
      'Standard example' => 'nodequeue_auto_rotate_cron',
    ],
    'aliases' => ['nqarc'],
  ];

  return $items;
}

/**
 * Callback function for drush nodequeue_auto_rotate_cron.
 */
function drush_nodequeue_auto_rotate_cron() {
  module_load_include('inc', 'nodequeue_auto_rotate');

  _nodequeue_auto_rotate();

  drush_log('drush_nodequeue_auto_rotate_cron: Cron->ok!!!');
}
