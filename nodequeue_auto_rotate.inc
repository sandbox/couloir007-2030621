<?php

/**
 * @file
 * A code for running drush commands.
 */

/**
 * Function to rotate nodequeue.
 */
function _nodequeue_auto_rotate() {
  // Select All Items that the rotate time has passed.
  $now = time();
  $query = db_select('nodequeue_auto_rotate', 'nrs')
    ->fields('nrs', [
      'qid',
      'rotate_start',
      'rotate_frequency',
      'rotate_frequency_units',
      'rotate_last',
      'rotate_scheduled',
    ])
    ->condition('rotate_start', $now, '<')
    ->execute();

  while ($row = $query->fetchObject()) {
    $do = FALSE;

    $subqueues = nodequeue_load_subqueues_by_queue($row->qid);

    if (empty(!$subqueues)) {
      if ($row->rotate_last == 0) {
        $response = date('M d, Y H:i:s', $now);
        $do = TRUE;
      }
      else {

        if ($row->rotate_scheduled) {
          switch ($row->rotate_frequency_units) {
            case 'min':
              $next = $row->rotate_last + ($row->rotate_frequency * 60);
              break;

            case 'hr':
              $next = $row->rotate_last + ($row->rotate_frequency * 3600);
              break;

            case 'day':
              $next = $row->rotate_last + ($row->rotate_frequency * 86400);
              break;
          }

          $response = date('M d, Y H:i:s', $next);

          if ($now >= $next) {
            drush_log('Queue ' . $row->qid . ' rotating now ' . $response, 'ok');
            $do = TRUE;
          }
          else {
            drush_log('Queue ' . $row->qid . ' rotate on ' . date('M d, Y H:i:s', $next), 'ok');
          }
        }
      }

      if ($do) {
        $subqueue = array_shift(array_values($subqueues));
        nodequeue_queue_front($subqueue, $subqueue->count);

        // Update the rotate_start to set the clock
        // ticking based on the time scheduled.
        db_update('nodequeue_auto_rotate')
          ->expression('rotate_last', ':rotate', ['rotate' => $now])
          ->condition('qid', $row->qid)
          ->execute();

        if ($row->rotate_scheduled) {
          $response = 'Queue ' . $row->qid . ' rotated, next scheduled rotation in ' . $row->rotate_frequency . ' ' . $row->rotate_frequency_units . ' on ' . $response;
          watchdog('noduqueue_rotate', $response, [], WATCHDOG_INFO);

          $response = 'Queue ' . $row->qid . ' rotating again in ' . $row->rotate_frequency . ' ' . $row->rotate_frequency_units . ' on ' . $response;
          drush_log($response, 'ok');
        }
        else {
          $response = 'Queue ' . $row->qid . ' rotated, Is a one time rotation.';
          watchdog('noduqueue_rotate', $response, [], WATCHDOG_INFO);

          $response = 'Queue ' . $row->qid . ' rotated, Is a one time rotation.';
          drush_log($response, 'ok');
        }
      }
    }
    else {
      drush_log('Queue ' . print_r($subqueues), 'ok');

      // Delete the entry when Noduequeue no longer exists.
      db_delete('nodequeue_auto_rotate')
        ->condition('qid', $row->qid)
        ->execute();
    }
  }
}
