Nodequeue Auto Rotate
=====================
This module is for autorotating nodequeues either once or on a set schedule.
Enabling this module adds a date popup field to the nodequeue edit form.
Administrators choose a date to start the rotation, the frequency of how
often to rotate, and what units the frequency is to occur, minutes,
hours, or days. When cron runs, the module checks the time the queue was
last rotated and will rotate if the allotted time has passed. When the queue
is rotated, the last node in the queue is moved position 1, all other nodes
are moved down one. This is ideal in a situation when, for instance, a view
is only displaying the first few nodes. A queue with 50 nodes displaying only
the top 5 with a frequency of 5 days can rotate for 50 days before
repeating itself.


REQUIREMENTS
=============
Nodequeue (https://www.drupal.org/project/nodequeue)
Date (https://www.drupal.org/project/date)


INSTALLATION
=============

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.



Configuration
=============
 1. On the queue administration page ("Administer >> Structure
    >> Nodequeues >> queue_name_edit") you need to:

    - Enable the rotate que on schedule for continuous, otherwise is set
      as a one off rotation.

    - Choose a starting date

    - Choose a frequency if it is to be more than a one time rotate.

    - Choose frequency units.


<h2>GIT</h2>
git clone --branch master couloir007@git.drupal.org:sandbox/couloir007/2030621.git nodequeue_auto_rotate




MAINTAINER
=============

Current maintainers:
 * Sean Montague (couloir007) - https://drupal.org/user/73794
